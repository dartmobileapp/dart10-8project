import 'dart:io';

int check(lottery) {
  if (lottery == 959594) {
    //1st
    return 1;
  } else if (lottery == 153165 || lottery == 852259) {//2nd
    return 2;
  } else {
    return 0;
  }
}

void main(List<String> args) {
  print('Enter your Lottery :');
  var lottery = int.parse(stdin.readLineSync()!);
  if (check(lottery) == 1) {
    print("$lottery won a 1st prize");
  } else if (check(lottery) == 2) {
    print("$lottery won a 2nd prize");
  } else {
    print('This Lottery ($lottery) is not won a prize');
  }
}
